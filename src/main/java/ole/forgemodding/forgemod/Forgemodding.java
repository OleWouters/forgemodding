package ole.forgemodding.forgemod;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import ole.forgemodding.forgemod.blocks.ModBlocks;
import ole.forgemodding.forgemod.items.ModItems;
import ole.forgemodding.forgemod.lib.Constants;
import ole.forgemodding.forgemod.world.WorldGeneratorCarrot;


@Mod(modid = Constants.MODID, name = Constants.MODNAME, version = Constants.VERSION)
public class Forgemodding {

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event){

        ModItems.init();
        ModBlocks.init();

        GameRegistry.registerWorldGenerator(new WorldGeneratorCarrot(), 1);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event){

    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event){

    }
}
