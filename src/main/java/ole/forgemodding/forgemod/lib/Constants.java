package ole.forgemodding.forgemod.lib;

public class Constants {
    public static final String MODID = "forgemodding";
    public static final String MODNAME = "Forge Mod";
    public static final String VERSION = "1.0";
}
