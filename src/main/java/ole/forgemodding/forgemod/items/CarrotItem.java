package ole.forgemodding.forgemod.items;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import ole.forgemodding.forgemod.lib.Constants;

public class CarrotItem extends Item {

    private String name ="ItemCarrot";

    public CarrotItem() {
        setUnlocalizedName(Constants.MODID + "_" + name);
        setCreativeTab(CreativeTabs.tabMaterials);
        GameRegistry.registerItem(this, name);
    }
}
