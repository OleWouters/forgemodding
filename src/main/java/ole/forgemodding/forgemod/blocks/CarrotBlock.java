package ole.forgemodding.forgemod.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import ole.forgemodding.forgemod.lib.Constants;

public class CarrotBlock extends Block {

    public String name = "CarrotBlock";
    public CarrotBlock(){

        super(Material.rock);
        this.setCreativeTab(CreativeTabs.tabBlock);
        setBlockName(Constants.MODID + "_" + name);
        GameRegistry.registerBlock(this, name);
    }

}
